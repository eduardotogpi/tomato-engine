These are but a few of the rules written for tomato-engine, mainly for
testing purposes. Lots more can be found [here](https://codeberg.org/eduardotogpi/tomato-rules)
