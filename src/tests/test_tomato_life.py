import numpy as np
import tomato as tt
from tomato.rules import tomato_life as rule

resources_path = "resources"
saved_state_matrix_path = f"{resources_path}/tomato_life.npy"
# initial_state_png_path =  f"{resources_path}/tiny_tomato.png"
initial_state_png_path = f"{resources_path}/tiny_tomato.png"


def run_generations(gens=10):
    # {{{
    # rng = np.random.default_rng(777)

    # state_matrix = rng.choice(2, size)

    board = tt.Board(
        rule,
        # max_fps=1,
        img_cell_size=1,
    )
    board.start(
        initial_state_png_path,
        show_window=False,
        generations=gens,
    )

    return board.state_matrix


# }}}


def save_generation(gens=10, *args, **kwargs):
    final_state_matrix = run_generations(gens, *args, **kwargs).astype("uint8")
    np.save(saved_state_matrix_path, final_state_matrix)


def load_generation():
    return np.load(saved_state_matrix_path)


def test_tomato_life():
    assert (load_generation() == run_generations(10)).all()
