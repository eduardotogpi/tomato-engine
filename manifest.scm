(use-modules (gnu packages)
             (gnu packages python)
             (gnu packages python-xyz)
             (gnu packages game-development)
             (guix packages)
             (guix download)
             (guix build-system python)
             (guix build-system pyproject)
             ((guix licenses) #:prefix license:))

(define-public python-tomato-engine
  (package
    (name "python-tomato-engine")
    (version "2.0.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "tomato-engine" version))
              (sha256
               (base32
                "1zg99gfxgr1vz54hvg4s8l1n1bgzc6wvfjl96abnipikrwhiqj4k"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-ipython python-numpy python-pillow
                             python-pygame))
    (home-page "https://codeberg.org/eduardotogpi/tomato-engine")
    (synopsis "Engine for prototyping and playing with cellular automata")
    (description "Engine for prototyping and playing with cellular automata")
    (license license:gpl3)))

(packages->manifest
  (list python python-tomato-engine))
